import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'task18-covidvaccination';
  
  candidateList:any = [
    'Pooja Bhisikar',
    'Tushar Manwatkar',
    'Nikita Chikhale',
    'Ankit Katkar',
    'Prajwal Madankar',
    'Abhijeet Dudhe',
    'Shivani Chhatre',
    'Neha Patil',
    'Kajal Dudhe',
    'Mohini Parate',
    'Ankit Jadhao'
  ];

  dose1:any = [];

  dose2:any = [];

  d1Done(index:any){
    this.dose1.push(this.candidateList[index]);
    this.candidateList.splice(index,1)
  }
  delete(index:any){
    this.candidateList.splice(index,1)
  }

  d2Done(index:any){
    this.dose2.push(this.dose1[index]);
    this.dose1.splice(index,1)
  }
  
  remove(index:any){
    this.candidateList.push(this.dose1[index]);
    this.dose1.splice(index,1) 
  }

  clear(index:any){
    this.dose1.push(this.dose2[index]);
    this.dose2.splice(index,1)
  }


}
